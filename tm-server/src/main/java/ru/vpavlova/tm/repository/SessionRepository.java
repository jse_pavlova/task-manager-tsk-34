package ru.vpavlova.tm.repository;

import ru.vpavlova.tm.api.repository.ISessionRepository;
import ru.vpavlova.tm.entity.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

}
